#!/usr/bin/env python3
# coding=utf-8

"""    pwn2dict 3.0 - converts PWN dictionaries to StarDict and tabfile formats
    Copyright (C) 2008-2021  Michał "mziab" Ziąbkowski

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."""

import sys
import zlib
import struct
import re

from argparse import ArgumentParser
from html import unescape
from functools import cmp_to_key
from pathlib import PurePath

# attempts to emulate the sort routine from stardict
def stardict_strcmp(x, y):
    pos = 0
    string_x = x[0].encode("utf-8")
    string_y = y[0].encode("utf-8")
    x_lower = string_x.lower()
    y_lower = string_y.lower()
    while (pos < len(string_x) and pos < len(string_y)):
        c1 = x_lower[pos]
        c2 = y_lower[pos]

        if c1 != c2:
            return c1-c2

        pos += 1

    # if words share common prefix, the shorter one takes precedence
    if pos < len(string_x):
        c1 = string_x[pos]
    else:
        c1 = 0

    if pos < len(string_y):
        c2 = string_y[pos]
    else:
        c2 = 0

    # special case: identical word, differing only in case
    # StarDict wants the ones with capital letters first
    if c1-c2 == 0:
        pos = 0
        while (pos < len(string_x) and pos < len(string_y)):
            if string_x[pos] != string_y[pos]:
                return string_x[pos]-string_y[pos]
            pos += 1

    return c1-c2

def b64_encode(val):
    b64_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    startfound = 0
    retval = ""

    for i in range(5, -1, -1):
        thispart = (val >> (6 * i)) & ((2 ** 6) - 1)
        if (not startfound) and (not thispart):
            # Both zero -- keep going.
            continue
        startfound = 1
        retval += b64_list[thispart]

    if retval:
        return retval

    return b64_list[0]

class PwnDict:
    # color table
    __colors = ["#736324", "#0000E0", "#E00000"]

    # defines what gets changed to what
    __replacements = [("&rsquo;", "'"),
                      ("<IMG SRC=\"ipa503.JPG\">", "\u02D0"),
                      ("&IPA502;", "\u02CC"),
                      ("&inodot;", "\u026A"),
                      ("<IMG SRC=\"schwa.JPG\">", "\u0259"),
                      ("<IMG SRC=\"ipa306.JPG\">", "\u0254"),
                      ("<IMG SRC=\"ipa313.JPG\">", "\u0252"),
                      ("<IMG SRC=\"IPAa313.JPG\">", "\u0252"),
                      ("<IMG SRC=\"ipa314.JPG\">", "\u028C"),
                      ("<IMG SRC=\"ipa321.JPG\">", "\u028A"),
                      ("<IMG SRC=\"ipa305.JPG\">", "\u0251"),
                      ("<IMG SRC=\"ipa326.JPG\">", "\u025C"),
                      ("<IMG SRC=\"ipa182.JPG\">", "\u0255"),
                      ("&##952;", "\u03B8"),
                      ("&##8747;", "\u0283"),
                      ("<SUB><IMG SRC=\"ipa135.JPG\"></SUB>", "\u0292"),
                      ("&eng;", "\u014B"),
                      ("&##39;", "'"),
                      ("<SUB><IMG SRC=\"rzym1.jpg\"></SUB>", "I"),
                      ("<SUB><IMG SRC=\"rzym2.jpg\"></SUB>", "II"),
                      ("<SUB><IMG SRC=\"rzym3.jpg\"></SUB>", "III"),
                      ("<SUB><IMG SRC=\"rzym4.jpg\"></SUB>", "IV"),
                      ("<SUB><IMG SRC=\"rzym5.jpg\"></SUB>", "V"),
                      ("<SUB><IMG SRC=\"rzym6.jpg\"></SUB>", "VI"),
                      ("<SUB><IMG SRC=\"rzym7.jpg\"></SUB>", "VII"),
                      ("<SUB><IMG SRC=\"rzym8.jpg\"></SUB>", "VIII"),
                      ("&square;", "\u2026"),
                      ("&quotlw;", "\""),
                      ("&quotup;", "\""),
                      ("<IMG SRC=\"idioms.JPG\">", "IDIOMS:"),
                      ("&squareb;", "\u2022"),
                      ("&hfpause;", "-"),
                      ("&tilde;", "~"),
                      ("&pause;", "\u2014"),
                      ("&##163;", "\u00A3"),
                      ("&dots;", "\u2026"),
                      ("&rArr;", "\u2192"),
                      ("&IPA118;", "\u0272"),
                      ("&##949;", "\u03B5"),
                      ("<IMG SRC=\"ipa183.JPG\">", "\u0291"),
                      ("&IPA413;", "\u031F"),
                      ("&IPA424;", "\u0303"),
                      ("&IPA505;", "\u0306"),
                      # added for PWN 2006/2007
                      ("&IPA135;", "\u0292"),
                      ("&IPA305;", "\u0251"),
                      ("&IPA306;", "\u0254"),
                      ("&IPA313;", "\u0252"),
                      ("&IPAa313;", "\u0252"),
                      ("&IPA314;", "\u028C"),
                      ("&IPA321;", "\u028A"),
                      ("&IPA326;", "\u025C"),
                      ("&IPA503;", "\u02D0"),
                      ("&IPA146;", "h"),
                      ("&IPA170;", "w"),
                      ("&IPA128;", "f"),
                      ("&IPA325;", "\u00E6"),
                      ("&IPA301;", "i"),
                      ("&IPA155;", "l"),
                      ("&IPA319;", "\u026A"),
                      ("&IPA114;", "m"),
                      ("&IPA134;", "\u0283"),
                      ("&IPA103;", "t"),
                      ("&IPA140;", "x"),
                      ("&IPA119;", "\u014B"),
                      ("&IPA131;", "\u00F0"),
                      ("&IPA130;", "\u03B8"),
                      ("&schwa.x;", "\u0259"),
                      ("&epsi;", "\u03B5"),
                      ("&ldquor;", "\u201C"),
                      ("&marker;", "\u2022"),
                      ("<SUB><IMG SRC=\"rzym9.jpg\"></SUB>", "IX"),
                      ("<SUB><IMG SRC=\"rzym10.jpg\"></SUB>", "X"),
                      ("<SUB><IMG SRC=\"rzym11.jpg\"></SUB>", "XI"),
                      ("<SUB><IMG SRC=\"rzym12.jpg\"></SUB>", "XII"),
                      ("<SUB><IMG SRC=\"rzym13.jpg\"></SUB>", "XIII"),
                      ("<SUB><IMG SRC=\"rzym14.jpg\"></SUB>", "XIV"),
                      ("<SUB><IMG SRC=\"rzym15.jpg\"></SUB>", "XV"),
                      ("&IPA101;", "p"),
                      ("&IPA102;", "b"),
                      ("&IPA104;", "d"),
                      ("&IPA109;", "k"),
                      ("&IPA110;", "g"),
                      ("&IPA116;", "n"),
                      ("&IPA122;", "r"),
                      ("&IPA129;", "v"),
                      ("&IPA132;", "s"),
                      ("&IPA133;", "z"),
                      ("&IPA153;", "j"),
                      ("&IPA182;", "\u0255"),
                      ("&IPA183;", "\u0291"),
                      ("&IPA302;", "e"),
                      ("&IPA304;", "a"),
                      ("&IPA307;", "o"),
                      ("&IPA308;", "u"),
                      ("&IPA309;", "y"),
                      ("&IPA322;", "\u0259"),
                      ("&IPA426;", "\u02E1"),
                      ("&IPA491;", "\u01EB"),
                      ("&IPA501;", "\u02C8"),
                      ("&comma;", ","),
                      ("&squ;", "\u2022"),
                      ("&ncaron;", "\u0148"),
                      ("&arabicrsquo.x;", "'"),
                      ("&atildedotbl.x;", "\u00E3"),
                      # Added for PWN Oxford 2005
                      ("<LITERA SRC=\"ipa135.JPG\">", "\u0292"),
                      ("<LITERA SRC=\"ipa305.JPG\">", "\u0251"),
                      ("<LITERA SRC=\"ipa306.JPG\">", "\u0254"),
                      ("<LITERA SRC=\"ipa313.JPG\">", "\u0252"),
                      ("<LITERA SRC=\"ipa314.JPG\">", "\u028C"),
                      ("<LITERA SRC=\"ipa321.JPG\">", "\u028A"),
                      ("<LITERA SRC=\"ipa326.JPG\">", "\u025C"),
                      ("<LITERA SRC=\"ipa503.JPG\">", "\u02D0"),
                      ("<LITERA SRC=\"schwa.JPG\">", "\u0259"),
                      ("<LITERA SRC=\"ipa182.JPG\">", "\u0255"),
                      ("<LITERA SRC=\"ipa183.JPG\">", "\u0291"),
                      ("&rdquor;", "\u201D"),
                      ("&verbar;", "|"),
                      ("&IPA405;", "\u0324"),
                      ("&idot;", "\u0130"),
                      ("&equals;", "="),
                      ("&vprime;", "'"),
                      ("&lsqb;", "\u005B"),
                      ("&rsqb;", "\u005D"),
                      # added for PWN Russian
                      ("&Acy;", "\u0410"),
                      ("&acy;", "\u0430"),
                      ("&Bcy;", "\u0411"),
                      ("&bcy;", "\u0431"),
                      ("&CHcy;", "\u0427"),
                      ("&chcy;", "\u0447"),
                      ("&Dcy;", "\u0414"),
                      ("&dcy;", "\u0434"),
                      ("&Ecy;", "\u042d"),
                      ("&ecy;", "\u044d"),
                      ("&Fcy;", "\u0424"),
                      ("&fcy;", "\u0444"),
                      ("&Gcy;", "\u0413"),
                      ("&gcy;", "\u0433"),
                      ("&HARDcy;", "\u042A"),
                      ("&hardcy;", "\u044A"),
                      ("&Icy;", "\u0418"),
                      ("&icy;", "\u0438"),
                      ("&IEcy;", "\u0415"),
                      ("&iecy;", "\u0435"),
                      ("&IOcy;", "\u0401"),
                      ("&iocy;", "\u0451"),
                      ("&Jcy;", "\u0419"),
                      ("&jcy;", "\u0439"),
                      ("&Kcy;", "\u041a"),
                      ("&kcy;", "\u043a"),
                      ("&KHcy;", "\u0425"),
                      ("&khcy;", "\u0445"),
                      ("&Lcy;", "\u041b"),
                      ("&lcy;", "\u043b"),
                      ("&Mcy;", "\u041c"),
                      ("&mcy;", "\u043c"),
                      ("&Ncy;", "\u041d"),
                      ("&ncy;", "\u043d"),
                      ("&numero;", "\u2116"),
                      ("&Ocy;", "\u041e"),
                      ("&ocy;", "\u043e"),
                      ("&Pcy;", "\u041f"),
                      ("&pcy;", "\u043f"),
                      ("&Rcy;", "\u0420"),
                      ("&rcy;", "\u0440"),
                      ("&Scy;", "\u0421"),
                      ("&scy;", "\u0441"),
                      ("&SHCHcy;", "\u0429"),
                      ("&shchcy;", "\u0449"),
                      ("&SHcy;", "\u0428"),
                      ("&shcy;", "\u0448"),
                      ("&SOFTcy;", "\u042C"),
                      ("&softcy;", "\u044C"),
                      ("&Tcy;", "\u0422"),
                      ("&tcy;", "\u0442"),
                      ("&TScy;", "\u0426"),
                      ("&tscy;", "\u0446"),
                      ("&Ucy;", "\u0423"),
                      ("&ucy;", "\u0443"),
                      ("&Vcy;", "\u0412"),
                      ("&vcy;", "\u0432"),
                      ("&YAcy;", "\u042f"),
                      ("&yacy;", "\u044f"),
                      ("&Ycy;", "\u042b"),
                      ("&ycy;", "\u044b"),
                      ("&YUcy;", "\u042e"),
                      ("&yucy;", "\u044e"),
                      ("&Zcy;", "\u0417"),
                      ("&zcy;", "\u0437"),
                      ("&ZHcy;", "\u0416"),
                      ("&zhcy;", "\u0436"),
                      ("&xutri;", "\u25B3"),
                      ("&diam;", "\u22C4"),
                      ("&Ccaron;", "\u010C"),
                      ("&rcaron;", "\u0159"),
                      ("&Omacr;", "\u014C"),
                      ("&Rcaron;", "\u0158"),
                      ("&ccaron;", "\u010D"),
                      ("&Zcaron;", "\u017D"),
                      ("&zcaron;", "\u017E"),
                      ("&abreve;", "\u0103"),
                      ("&umacr;", "\u016B"),
                      ("&tcedil;", "\u0162"),
                      ("&ecaron;", "\u011B"),
                      ("&scedil;", "\u015F"),
                      ("&omacr;", "\u014D"),
                      ("&amacr;", "\u0101"),
                      ("&uring;", "\u016F"),
                      ("&amp;lt;", "\u2039"),
                      ("&amp;gt;", "\u203A"),
                      ("&amp;amp;", "&"),
                      ("&lt;", "\u2039"),
                      ("&lstrok;", "\u0142"),
                      ("&eogon;", "\u0119"),
                      ("&nacute;", "\u0144"),
                      ("&sacute;", "\u015B"),
                      ("&cacute;", "\u0107"),
                      ("&aogon;", "\u0105"),
                      ("&zdot;", "\u017C"),
                      ("&Sacute;", "\u015A"),
                      ("&Zdot;", "\u017B"),
                      # added for SWO and SF
                      ("&dash;", "\u2010"),
                      ("<IMG SRC=\"ksiazecz.bmp\">", ""),
                      ("&utilde;", "\u0169"),
                      ("&upsi;", "\u03C5"),
                      ("&Upsi;", "\u03D2"),
                      ("&percnt;", "%"),
                      ("&phiv;", "\u03C6"),
                      ("&star;", "\u2606"),
                      ("&gt;", "\u203A"),
                      ("&omacrac.x;", "\u1E53"),
                      ("&emacr;", "\u0113"),
                      ("&emacrac.x;", "\u1E17"),
                      ("&bullb;", "\u2022"),
                      ("&wcirc;", "\u0175"),
                      ("&mdot.x;", "\u1E41"),
                      ("&imacr;", "\u012B"),
                      ("&gmacr.x;", "\u1E21"),
                      ("&tdotbl.x;", "\u1E6D"),
                      ("&gcaron.x;", "\u01E7"),
                      ("&hdotbl.x;", "\u1E25"),
                      ("&wdot;", "\u1E87"),
                      ("&lsquor;", "\u201A"),
                      ("&cdot;", "\u010B"),
                      ("&bdot;", "\u1E03"),
                      ("&sdotbl.x;", "\u1E63"),
                      ("&ndotbl.x;", "\u1E47"),
                      ("&acaron.x;", "\u01CE"),
                      ("&ndot.x;", "\u1E45"),
                      ("&kdot;", "\uE568"),
                      ("&amacrac.x;", "\uE40A"),
                      ("&Emacr;", "\u0112"),
                      ("&Hdotbl.x;", "\u1E24"),
                      ("&rringbl.x;", "\uE6A3"),
                      ("&etilde.x;", "\u1EBD"),
                      ("&Imacr;", "\u012A"),
                      ("&ddotbl.x;", "\u1E0D"),
                      ("&ucaron.x;", "\u01D4"),
                      ("&rdotbl.x;", "\u1E5B"),
                      ("&gdot;", "\u0121"),
                      ("&itilde;", "\u0129"),
                      ("&iumlacute;", "\u1E2F"),
                      ("&imacrtilde;", "\u012B"),
                      ("&eibrevebl;", "\u1EB5"),
                      ("&hmacrbl.x;", "\u1E96"),
                      ("&nmacr.x;", "\u0304"),
                      ("&tmacrbl.x;", "\u1E6F"),
                      ("&dmacrbl.x;", "\u1E0F"),
                      ("&umacrtilde;", "\u0169"),
                      ("&Hmacrbl.x;", "Kh"),
                      ("&zdotbl.x;", "\u1E93"),
                      ("&edot;", "\u0117"),
                      ("&dolnagw;", "\u2606"),
                      # PWN 2003
                      ("&s224;", "\u25CA"),
                      ("&karop;", "\u25CA"),
                      ("&s225;", "\u2329"),
                      ("&s241;", "\u232A"),
                      ("&#!0,127;", "\u25AB"),
                      ("&##37;", "%"),
                      ("&##9553;", "\u2551"),
                      ("&oboczn;", "\u2551"),
                      ("&##1100;", "\u044c"),
                      ("&##1098;", "\u044a"),
                      ("<PRZYSL I=przysl>", "przysł."),
                      ("&s172;", "\u2190"),
                      ("<ODSTEP ROZMIAR=80>", ""),
                      ("</ODSTEP>", ""),
                      ("<P STYLE=\"LISTA_ARAB\">", ""),
                      ("<P STYLE=\"LISTA_KAPI\">", ""),
                      ("<P STYLE=\"LISTA_RZYM\">", ""),
                      ("<P STYLE=\"ELEM_RZYM\">", ""),
                      ("&ytilde;", "\u1ef9"),
                      ("&estrok;", "\u011b"),
                      ("&ndotbl;", "\u1E47"),
                      ("&yogh;", "\u021D"),
                      ("&ismutne;", "i"),
                      ("&usmutne;", "u"),
                      ("&middot_s;", "\u22C5"),
                      ("<IMG SRC=\"IPA313.JPG\">", "\u0252"),
                      ("<IMG SRC=\"IPA503.JPG\">", "\u02D0"),
                      ("<IMG SRC=\"IPA314.JPG\">", "\u028C"),
                      ("<IMG SRC=\"IPA306.JPG\">", "\u0254"),
                      ("<IMG SRC=\"IPA321.JPG\">", "\u028A"),
                      ("<IMG SRC=\"IPA305.JPG\">", "\u0251"),
                      ("<IMG SRC=\"IPA135.JPG\">", "\u0292"),
                      ("<IMG SRC=\"IPA326.JPG\">", "\u025C"),
                      ("<IMG SRC=\"IPAa126.JPG\">", "\u0278"),
                      ("&##133;", "\u2026"),
                      # German-Polish, Polish-German
                      ("&semi;", "\u003B"),
                      ("&zacute;", "\u017A"),
                      ("&dollar;", "$"),
                      ("&frac13;", "\u2153"),
                      ("&frac15;", "\u2155"),
                      ("&ldotbl.x;", "\u1E37"),
                      ("&mdotbl.x;", "\u1E43"),
                      ("&commat;", "\u0040"),
                      ("&Lstrok;", "\u0141"),
                      ("&Aogon;", "\u0104"),
                      ("&ybreve.x;", "\uE376"),
                      ("&IPA177;", "\u01C0"),
                      ("&IPA324;", "\u0250"),
                      ("&IPA432;", "\u032F"),
                      ("&IPA432i;", "i\u032F"),
                      ("&IPA303;", "\u025B"),
                      ("&IPA138;", "\u00E7"),
                      ("&IPA320;", "\u028F"),
                      ("&IPA214;", "\u02A4"),
                      ("&epsilontilde;", "\uEB23"),
                      ("&oslashtilde;", "\u00F8\u0303"),
                      ("&engdotabove;", "\u014B\u0307"),
                      ("&auluk;", "au"),
                      ("&ailuk;", "ai"),
                      ("&cyluk;", "\u0254y"),
                      ("&uibrevebl;", "\u032f"),
                      ("&ai1luk;", "a\u026a"),
                      ("&ei1luk;", "e\u026a"),
                      ("<GOBACK>", ""),
                      ("&brvbar;", "|"),
                      # needs to be done here
                      ("&ap;~", "\u2248"),
                      ("&ap;", "\u2248")
                      ]

    __replacements_dict = dict(__replacements)
    __replacements_regex = re.compile("|".join((re.escape(x[0]) for x in __replacements)))

    # converts and cleans up definitions/words
    def __format(self, temp):
        # convert symbols to unicode
        temp = re.sub(self.__replacements_regex, lambda x: self.__replacements_dict[x.group(0)], temp)

        # unescape standard HTML entities
        temp = unescape(temp)

        # some clean-up
        temp = re.sub("^<BIG>.*?</BIG> ?", "", temp)            # remove entry name, we already have it
        temp = re.sub("<ICON.*?>", "", temp)
        temp = re.sub("<P> ?</P>", "", temp)                # remove redundant <P>s
        temp = temp.replace("<P>", "<br><br>")              # change <P>s to linebreaks
        temp = temp.replace("</P><B>", "<br><br><B>")           # add linebreak before some bold words
        temp = temp.replace("</P>", "")
        temp = temp.replace("<HANGINGPAR>", "")
        temp = re.sub("<TEXTSECTION.*?>", "", temp)
        temp = re.sub("</?PL>", "", temp)
        temp = re.sub("</?GB>", "", temp)
        temp = re.sub("<SUP>(.*?)</SUP>", " \\1", temp)
        temp = re.sub("<HMS NR=\"(.*?)\">", " \\1", temp)
        # added for PWN 2006/2007
        temp = re.sub("</?STEM>", "", temp)
        temp = re.sub("</?PH>", "", temp)
        # added for PWN Russian
        temp = re.sub("</?RU>", "", temp)
        temp = re.sub("<IGNORE>.*?</IGNORE>", "", temp)
        temp = re.sub("<IGNORE>.*", "", temp)               # leftovers in rus-pol
        # added for PWN German
        temp = re.sub("</?DE>", "", temp)
        temp = re.sub("<WMF.*?>", "", temp)
        temp = re.sub("</?BOOKMARK>", "", temp)
        temp = re.sub("<TABLEREF.*?>", "", temp)

        # fix Polish-English ugliness
        temp = temp.replace("\n", "<br>")
        temp = re.sub("</?A.*?>", "", temp)
        temp = re.sub("^<br><br> ?", "", temp)

        # some tweaks for PWN 2003
        temp = temp.replace("<P STYLE=\"TAB\">", "<br>")
        temp = temp.replace("<br><br> <br><br>", "<br><br>")
        temp = re.sub("<IMG SRC=\".*?\" DATA=\"(.*?)\">", "<SMALL>\\1</SMALL>", temp)
        temp = temp.replace("<br><br><BR> <br><br>", "<br><br>")

        # add some color, if requested
        if self.__use_colors:
            temp = re.sub("<I>( [^<]*)</I>",
                          "<I><font color=\"" + self.__colors[0] + "\">\\1</font></I>", temp)
            temp = re.sub(r"<I>([^<\[]*)</I>",
                          "<I><font color=\"" + self.__colors[1] + "\">\\1</font></I>", temp)
            temp = re.sub("([^(])<SMALL>([^<]*)</SMALL>",
                          "\\1<font color=\"" + self.__colors[2] + "\">\\2</font>", temp)

        # remove leading and trailing whitespace
        temp = temp.strip()

        return temp

    def write_stardict(self, fname):
        book_name = PurePath(fname)

        with open(fname, "w", encoding="utf-8") as dictionary:
            with open(book_name.with_suffix(".idx"), "wb") as idx:
                for word, addr in self.words:
                    definition = self.read_definition(addr)
                    offset = dictionary.tell()
                    size = len(definition.encode("utf-8"))

                    # write definition to .dict
                    dictionary.write(definition)

                    # write null-terminated word, offset and size to .idx
                    idx.write(word.encode("utf-8") + b"\x00")
                    idx.write(struct.pack(">2I", offset, size))

                idx_size = idx.tell()

        # write .ifo
        with open(book_name.with_suffix(".ifo"), "wb") as ifo:
            header = "StarDict's dict ifo file\n"
            header += "version=3.0.0\n"
            header += "idxoffsetbits=32\n"
            header += f"wordcount={len(self.words)}\n"
            header += f"idxfilesize={idx_size}\n"
            header += f"bookname={book_name.stem}\n"
            header += "sametypesequence=h\n"
            ifo.write(header.encode("utf-8"))

    def write_tabfile(self, fname):
        with open(fname, "w", encoding="utf-8") as tab:
            for word, addr in self.words:
                tmp = self.read_definition(addr)

                if tmp != "":
                    tab.write(f"{word}\t{tmp}\n")

    def write_dictd(self, fname):
        book_name = PurePath(fname)

        with open(fname, "w", encoding="utf-8") as dictionary:
            with open(book_name.with_suffix(".index"), "w", encoding="utf-8") as idx:

                for word, definition in [("00-database-utf8", ""), ("00-database-short", book_name)]:
                    offset = dictionary.tell()
                    size = len(word)
                    dictionary.write(definition)
                    idx.write(f"{word}\t{b64_encode(offset)}\t{b64_encode(size)}\n")

                for word, addr in self.words:
                    definition = self.read_definition(addr)
                    offset = dictionary.tell()
                    size = len(definition.encode("utf-8"))

                    # write definition to .dict
                    dictionary.write(definition)

                    # write tab-separated word, offset and size to .index
                    idx.write(f"{word}\t{b64_encode(offset)}\t{b64_encode(size)}\n")

    def read_definition(self, addr):
        # read definition
        self.__dict.seek(addr)
        word_buffer = self.__dict.read(30000)
        temp = b""
        first_byte = word_buffer[0]

        if first_byte == 0:
            word_buffer = word_buffer[1:]
            first_byte = word_buffer[0]

        if first_byte < 20:
            # take pointer and decompress
            temp = zlib.decompress(word_buffer[first_byte + 1:])
        else:
            # copy up to next null char
            pos = word_buffer.index(b"\x00")
            temp = word_buffer[:pos]

        cur = temp.decode(self.__encoding, "replace")
        return self.__format(cur)

    def __init__(self, dict_name, use_colors=False):
        self.__dict = open(dict_name, "rb")
        self.__use_colors = use_colors

        # temporary variables
        wordcount = 0
        index_base = 0
        words_base = 0
        header_offset = 2

        # check header
        header = struct.unpack("<I", self.__dict.read(4))[0]

        # seek and read data
        if header in [0x81125747, 0x81115747]:
            print("Detected Oxford PWN 2003/2004 format...")
            self.__encoding = "cp1250"

            # checking for alternative 2005 format
            if struct.unpack("<I", self.__dict.read(4))[0] == 2:
                header_offset = 5

            self.__dict.seek(0x18)
            wordcount, index_base, words_base = struct.unpack("<3I", self.__dict.read(12))
        elif header == 0x81135747:
            print("Detected PWN 2005 format...")
            self.__encoding = "iso-8859-2"
            self.__dict.seek(0x18)
            wordcount, index_base, words_base = struct.unpack("<3I", self.__dict.read(12))
        elif header == 0x81145747:
            print("Detected Oxford PWN 2006/2007 format...")
            self.__encoding = "iso-8859-2"
            self.__dict.seek(0x68)
            wordcount, index_base, _, words_base = struct.unpack("<4I", self.__dict.read(16))
        else:
            raise ValueError("Incorrect file header")

        offsets = []
        self.words = []

        # read alphabetical index
        print("Reading alphabetical index...")

        self.__dict.seek(index_base)
        for i in range(wordcount):
            addr = struct.unpack("<I", self.__dict.read(4))[0] & 0x07ffffff
            offsets += [addr]

        # read index table
        print("Reading words...")

        for i in range(wordcount):
            word_start = words_base + offsets[i]
            self.__dict.seek(word_start)
            word_buffer = self.__dict.read(300)
            entry_type = word_buffer[2 + 1]

            # 0x49, 0x38, 0x76 = information entries
            if entry_type not in [0x49, 0x38, 0x76, 0x18, 0x28, 0x40]:
                word_buffer = word_buffer[2 + 4 + 6:]
                length = word_buffer.index(b"\x00")
                word = word_buffer[:length]

                word = word.decode(self.__encoding)

                # formatting is very costly, so bail out if it's not needed
                if re.search("[<&;]", word):
                    word = self.__format(word)

                word = word.replace("|", "")        # make Polish->English words nicer to look at
                word = re.sub(r" \d+$", "", word)   # remove word-final numbers
                word = re.sub("</?.*?>", "", word)  # strip html tags
                word = word.rstrip()

                self.words += [(word, length + header_offset + word_start + 2 + 4 + 6)]

        # sort words, since stardict requires it
        self.words.sort(key=cmp_to_key(stardict_strcmp))

class MyArgumentParser(ArgumentParser):
    def error(self, message):
        self.print_help(sys.stderr)
        args = {'prog': self.prog, 'message': message}
        self.exit(2, ('\n%(prog)s: error: %(message)s\n') % args)

    def format_help(self):
        self._action_groups = self._action_groups[1:]
        self._action_groups[0].title = 'Options'
        return super().format_help()

if __name__ == "__main__":
    # parse options
    usage = "%(prog)s [options] pwn_file.win [stardict_file.dict]"
    parser = MyArgumentParser(usage=usage)
    parser.add_argument("in_file")
    parser.add_argument("out_file", nargs="?", default=None)
    parser.add_argument("-t", "--tabfile", action="store_true", dest="tabfile",
                        help="convert to tabfile format instead")
    parser.add_argument("-d", "--dictd", action="store_true", dest="dictd",
                        help="convert to dictd format instead")
    parser.add_argument("-c", "--colors", action="store_true", dest="colors",
                        help="add some colors for readability (experimental)")
    options = parser.parse_args()

    # set up variables for formats
    if options.tabfile:
        mode = "tabfile"
        ext = ".tab"
    elif options.dictd:
        mode = "dictd"
        ext = ".dict"
    else:
        mode = "stardict"
        ext = ".dict"

    # make output filename if not specified
    if options.out_file is not None:
        dest_name = options.out_file
    else:
        dest_name = PurePath(options.in_file).with_suffix(ext)

    src_dict = PwnDict(options.in_file, use_colors=options.colors)

    # convert and write data
    print("Writing converted data...")

    if mode == "stardict":
        src_dict.write_stardict(dest_name)
    elif mode == "tabfile":
        src_dict.write_tabfile(dest_name)
    elif mode == "dictd":
        src_dict.write_dictd(dest_name)
